public class SistemaDeVendas {

    private MaquinaDeIngressos maquina;

    public void rodar() {
        maquina = new MaquinaDeIngressos(10, 20, "Cinema 5D");

        maquina.comprarIngresso(10.5);
        maquina.comprarIngresso(14);
        maquina.comprarIngresso(2);

        maquina.consultarSaldo();
        maquina.imprimirRelatorio();
    }

    public static void main(String args[]) {
        new SistemaDeVendas().rodar();
    }
}