public class MaquinaDeIngressos {
    private double valor;
    private int totalIngressosVendidos;
    private double saldo;
    private int totalIngressos;
    private String nomeDoEvento;
    private double troco;

    public MaquinaDeIngressos(double valor, int totalIngressos, String nomeDoEvento) {
        this.valor = valor;
        this.totalIngressos = totalIngressos;
        this.nomeDoEvento = nomeDoEvento;
    }

    public void comprarIngresso(double valorPago) {
        if (totalIngressos >= 1) {
            if (valorPago >= this.valor) {
                troco = valorPago - valor;
                saldo = saldo + this.valor;
                totalIngressos--;
                totalIngressosVendidos++;
                this.imprimirIngresso();
                System.out.println("Troco = "+troco);
            } else {
                System.out.println("Valor insuficiente para realizar a compra.");
            }
        } else {
            System.out.println("Não há ingressos disponíveis para venda.");
        }
    }

    public void consultarSaldo() {
        System.out.println("Saldo = "+saldo);
    }

    public void imprimirRelatorio() {
        System.out.println("\nRelatório de Vendas");
        System.out.println("-------------------");
        System.out.println("Evento: "+nomeDoEvento);
        System.out.println("Valor do Ingresso: "+valor);
        System.out.println("Total de Ingressos Vendidos: "+totalIngressosVendidos);
        System.out.println("Saldo das Vendas: "+saldo);
    }

    private void imprimirIngresso() {
        System.out.println("\nIngresso");
        System.out.println("---------");
        System.out.println("Evento: "+nomeDoEvento);
        System.out.println("Valor = "+valor);
    }
}